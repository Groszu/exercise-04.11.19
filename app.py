class Mammal:
    def __init__(self):
        print("I'm alive")

    def move(self):
        print("I'm moving")

    age = 0


class Dog(Mammal):
    def __init__(self):
        super().__init__()
        print("I'm a dog")

    def bark(self):
        print("Woof woof!")


def write_name():
    print("Radoslaw")


def add(a, b, c):
    print(a+b+c)


def hello_world() -> str:
    return "Cześć świecie"


def sub(a, b) -> float:
    return a - b


write_name()
add(10, 20, 30)
print(hello_world())
print(sub(10, 5))
dog = Dog()
print(dog.age)
dog.move()

